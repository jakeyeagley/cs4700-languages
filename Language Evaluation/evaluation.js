//This is a JavaScript program that encodes and decodes strings
//
//Jake Yeagley
//CS4700
//

function wordCount(str)
{
	if(str == "" || str == " ")
		 return 0;
	return str.split(" ").length;
}

var map = [ 
		'A','b','C','d','E','f','G','h','I','j','K','l','M','n','O','p','Q','r','S','t','U','v','W','x','Y','z','a','B','c','D','e','F','g','H','i','J','k','L','m','N','o','P','q','R','s','T','u','V','w','X','y','Z', 
];

function encode(str)
{
	var length = str.length;
	var encodedNum = [];
	var encodedStr = [];
	for(var i = 0; i < length; i++)
	{
		if(str.charAt(i) == " ")
		{
			//var temp = length % i;
			encodedNum[i] = 32;
			//print(encodedNum[i]);
			//print((encodedNum[i] - 15).toString(36));
		}
		else
		{
			encodedNum[i] = str.charCodeAt(i) - 96;
			//encodedNum[i] += 3;
			//print("encoded: " + encodedNum[i]);
			//print((encodedNum[i] + 9).toString(36));	
		}
		encodedStr[i] = map[encodedNum[i]];
		//print("map encoded at number: " + i + " "+map[encodedNum[i]]);
		//print(encodedStr[i]);
	}
	encodedStr.toString();
	str = "";
	for(var i = 0; i < length; i++)
	{
		str += encodedStr[i];
		//print("encodedStr[" + i + "]: " + encodedStr[i]);
	}
	//print(str);
	return str;
};

function decode(str)
{
	var length = str.length;
	var decodedNum = [];
	var decodedStr = [];
	
	str = str.toLowerCase();
	
	for(var i = 0; i < length; i++)
	{
		if(str.charCodeAt(i) - 97 == 6)
			decodedNum[i] = 32;
		else 
			decodedNum[i] = str.charCodeAt(i) - 97;
		//print("decoded: " + decodedNum[i]);
	}

	for(var i = 0; i < length; i++)
	{
		decodedStr[i] = map[decodedNum[i] - 1];
		if(decodedStr[i] == "F")
			decodedStr[i] = " ";
	}
	
	decodedStr.toString();
	str = "";
	for(var i = 0; i < length; i++)
	{
		str += decodedStr[i];
		//print("encodedStr[" + i + "]: " + decodedStr[i]);
	}
	
	return str.toLowerCase();
}
var sentence1 = "i am a student at utah state university";
var sentence2 = "my major is computer science";
var sentence3 = "this is a test to see how different strings are encoded";


print("sentence 1: " + sentence1);
sentence1 = encode(sentence1);
print("encoded: " + sentence1);
sentence1 = decode(sentence1);
print("decoded: " + sentence1);
print("word count is:" + wordCount(sentence1));


print("sentence 2: " + sentence2);
sentence2 = encode(sentence2);
print("encoded: " + sentence2);
sentence2 = decode(sentence2);
print("decoded: " + sentence2);
print("word count is:" + wordCount(sentence2));


print("sentence 3: " + sentence3);
sentence3 = encode(sentence3);
print("encoded: " + sentence3);
sentence3 = decode(sentence3);
print("decoded: " + sentence3);
print("word count is:" + wordCount(sentence3));


