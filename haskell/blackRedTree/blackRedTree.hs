data Color = Red | Black deriving Show
data Tree x = Empty | Node Color (Tree x) x (Tree x) deriving Show

insert (Empty) val = Node Red (Empty) val (Empty)
insert (Node c left val right) x
    | x == val = Node c left val right
    | x < val = Node c (balance(insert left x)) val right
    | x > val = Node c left val (balance(insert right x))

makeBlack (Node Red l v r) = Node Black l v r
makeBlack t = t
treeInsert t v = makeBlack(balance(insert t v)) 

height Empty = 0
height (Node _ l _ r) = (max (height l)  (height r)) + 1

balance (Node Black a x (Node Red (Node Red b y c) z d)) = (Node Red (Node Black a x b) y (Node Black c z d))
balance (Node Black (Node Red a x (Node Red b y c)) z d) = (Node Red (Node Black a x b) y (Node Black c z d))
balance (Node Black (Node Red (Node Red a x b) y c) z d) = (Node Red (Node Black a x b) y (Node Black c z d))
balance (Node Black a x (Node Red b y (Node Red c z d))) = (Node Red (Node Black a x b) y (Node Black c z d))
balance t = t

