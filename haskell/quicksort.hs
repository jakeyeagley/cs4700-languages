qsort [] = [] 
qsort(head:tail) =
    qsort [less | less <- tail , less <= head] 
    ++ [head] 
    ++ qsort [greater | greater <- tail , greater > head]

