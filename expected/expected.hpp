#ifndef EXPECTED_H
#define EXPECTED_H

#include <functional>
#include <exception>

template<typename T>
class Expected 
{
    public:
		T t;
		std::exception_ptr e;	
		bool valid;

		Expected() {};
        Expected(T tx):valid(true){
            t = tx;
        }
        Expected(std::exception_ptr ex):valid(false){
            e = ex;
        }
        Expected(std::exception ex):valid(false){
            e = make_exception_ptr(ex);
        }
        T value() 
        {
            if(valid) return t;
            std::rethrow_exception(e);
        }
        operator T()
        {
            return value();
        }
		
		Expected<int> apply()
		{
		 if(!valid) return e;
            try
            {
				Expected<int> expected;
                return expected;
            }
            catch(...)
            {
				Expected<int> expected;
				expected.e = std::current_exception();
                return expected;
			}
		}

        template<typename U>
        Expected<U> apply(std::function<U (T)> f)
        {
            if(!valid) return e;
            try
            {
				Expected<U> expected;
				expected.t = f();
                return expected;
            }
            catch(...)
            {
				Expected<U> expected;
				expected.e = std::current_exception();
                return expected;
            }
        }
    private:
};

template<typename T, typename U> 
auto operator+(Expected<T> t,U u) 
{
    return t.apply([u](T t){return t + u;});
}

template<typename T, typename U>
auto operator+(T t, Expected<U> u)
{
    return u.apply([t](U u){return u + t;});
}

template<typename T, typename U>
auto operator-(T t, Expected<U> u)
{
    return u.apply([t](U u){return u - t;});
}

template<typename T, typename U>
auto operator-(Expected<T> t,U u)
{
    return u.apply([t](U u){return u - t;});
}

template<typename T, typename U> 
auto operator<(Expected<T> t,U u) 
{
    return t.apply([u](T t){return t < u;});
}

template<typename T, typename U>
auto operator<(T t, Expected<U> u)
{
    return u.apply([t](U u){return u < t;});
}

template<typename T, typename U>
auto operator>(T t, Expected<U> u)
{
    return u.apply([t](U u){return u > t;});
}

template<typename T, typename U>
auto operator>(Expected<T> t,U u)
{
    return u.apply([t](U u){return u > t;});
}

template<typename T, typename U> 
auto operator/(Expected<T> t,U u) 
{
    return t.apply([u](T t){return t / u;});
}

template<typename T, typename U>
auto operator/(T t, Expected<U> u)
{
    return u.apply([t](U u){return u / t;});
}

template<typename T, typename U>
auto operator*(T t, Expected<U> u)
{
    return u.apply([t](U u){return u * t;});
}

template<typename T, typename U>
auto operator*(Expected<T> t,U u)
{
    return u.apply([t](U u){return u * t;});
}

template<typename T, typename U> 
auto operator%(Expected<T> t,U u) 
{
    return t.apply([u](T t){return t % u;});
}

template<typename T, typename U>
auto operator%(T t, Expected<U> u)
{
    return u.apply([t](U u){return u % t;});
}

template<typename T, typename U>
auto operator==(T t, Expected<U> u)
{
    return u.apply([t](U u){return u == t;});
}

template<typename T, typename U>
auto operator==(Expected<T> t,U u)
{
    return u.apply([t](U u){return u == t;});
}

template<typename T, typename U> 
auto operator<=(Expected<T> t,U u) 
{
    return t.apply([u](T t){return t <= u;});
}

template<typename T, typename U>
auto operator<=(T t, Expected<U> u)
{
    return u.apply([t](U u){return u <= t;});
}

template<typename T, typename U>
auto operator>=(T t, Expected<U> u)
{
    return u.apply([t](U u){return u >= t;});
}

template<typename T, typename U>
auto operator>=(Expected<T> t,U u)
{
    return u.apply([t](U u){return u >= t;});
}

#endif //EXPECTED_H
