#include <iostream> 
#include <functional>

//two constructors
//T->M(T)
//T<-M(T)
//(T->U) -> (M(T) -> M(U))
//E<T> t
//t.apply(f) an expected U
//std::function
//template<typename T, typename U>
//E<U> E<T>::apply(std::function<U(T) f>
//std::exception_ptr
//std::current_exception() put in try block
//operater+(E<T>a,E<T>b)
//try
//auto B = b.value();
//a.apply([B](T t){return t + B;}); 
//catch(...)
//return current_exception

//e<int> a = 3
//auto b = a.apply(static_cast<double>); b is a e<double> 

template<class T>
class Expected
{
    public:
    Expected(T t){};
    Expected(std::exception_ptr eptr){};
    Expected<T> apply(std::function<T(T)> f)
    {   
       bool hasT = false; 
       {
            try
            {
                    Expected<T> value = f(t);
                    return value;
            }
            catch(...)
            {
                    return std::current_exception();
            }
       }
    Expected<T>& operator+(const T& b); 
};

template<typename U>
Expected<U>& Expected<U>::operator+(const U& b)
{
    std::exception_ptr eptr;    
    try 
    {
        Expected<U> B = b.value();
        this.apply([B](U u){return u + B;});
    }        
    catch(...)
    {
        eptr = std::current_exception();
    }        
    return eptr;
}
//operator T()
//{
//    if(valid)
//        return value.t;
//    else
//        return std::return(value.t);    
//}

int main()
{
    Expected<int> e;   
    
    return 0;
}
