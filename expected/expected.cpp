#include <iostream>
#include "expected.hpp"

int function(int a)
{
	return a;
}
int main()
{
	Expected<int> eI(2);
	Expected<int> e2(3);
	Expected<int> eD(std::exception);
	int a = 2;
	int b = 3;
	
	std::cout << eI.t << std::endl << std::endl;
	
	eI.apply();
	
    return 0;
}

